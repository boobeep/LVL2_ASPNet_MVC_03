﻿using LVL2_ASPNet_MVC_03.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LVL2_ASPNet_MVC_03.Controllers
{
    public class CustomerController : Controller
    {
        db_customerEntities db = new db_customerEntities();
        // GET: Customer
        public ActionResult Index()
        {
            return View(db.tbl_customer.ToList());
        }

        // GET: Customer/Details/5
        public ActionResult Details(int id)
        {
            return View(db.tbl_customer.Where(x=>x.Id == id).FirstOrDefault());
        }

        // GET: Customer/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Customer/Create
        [HttpPost]
        public ActionResult Create(tbl_customer customer)
        {
            /*
            try
            {
                int a = 10;
                int b = 2; 
                int c = a / b;

            }
            catch (Exception msg)
            {
                ViewBag.Message = msg;
            }*/
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    // TODO: Add insert logic here
                    //Insert data baru [Proses 1]
                    db.tbl_customer.Add(customer);
                    db.SaveChanges();

                    //[Proses 3]
                    int a = 10;
                    int b = 0;
                    int c = a / b;

                    //Update address with name Agung [Proses 2]
                    tbl_customer edit = db.tbl_customer.Where(x => x.Id == 1).FirstOrDefault();
                    edit.address = "Kota Bandung";
                    db.Entry(edit).State = EntityState.Modified;
                    db.SaveChanges();
                    transaction.Commit();
                    return RedirectToAction("Index");
                }
                catch (Exception msg)
                {
                    transaction.Rollback();
                    ViewBag.Message = msg;
                    return View();
                }
            }
        }

        // GET: Customer/Edit/5
        public ActionResult Edit(int id)
        {
            return View(db.tbl_customer.Where(x=>x.Id == id).FirstOrDefault());
        }

        // POST: Customer/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, tbl_customer customer)
        {
            try
            {
                // TODO: Add update logic here
                db.Entry(customer).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Customer/Delete/5
        public ActionResult Delete(int id)
        {
            return View(db.tbl_customer.Where(x=>x.Id == id).FirstOrDefault());
        }

        // POST: Customer/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, tbl_customer customer)
        {
            try
            {
                // TODO: Add delete logic here
                customer = db.tbl_customer.Where(x => x.Id == id).FirstOrDefault();
                db.tbl_customer.Remove(customer);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
